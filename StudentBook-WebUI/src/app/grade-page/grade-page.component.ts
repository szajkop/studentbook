import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-grade-page',
  templateUrl: './grade-page.component.html',
  styleUrls: ['./grade-page.component.css']
})
export class GradePageComponent implements OnInit {

  private httpClient: HttpClient;
  public grades?: Grade[];

  constructor(http: HttpClient) {
      this.httpClient = http;
  }

  ngOnInit(): void {
    this.httpClient.get<Grade[]>('/api/Student/GetStudentStatistics').subscribe(result => {
        this.grades = result;
    }, error => console.error(error));
  }

}


interface Grade {
  name: string;
  gradesAvg: number;
  bestGrade: number;
  failingGradeCount: number
}
