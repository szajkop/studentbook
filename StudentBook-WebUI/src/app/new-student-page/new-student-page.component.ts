import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-new-student-page',
  templateUrl: './new-student-page.component.html',
  styleUrls: ['./new-student-page.component.css']
})
export class NewStudentPageComponent implements OnInit {

  public name: string = '';
  public semester: number = 0;
  birthDate: string = "";
  phoneNumber: string = "";

  private httpClient: HttpClient;
  private router: Router;

  constructor(http: HttpClient, router: Router) 
  { 
    this.httpClient = http;
    this.router = router;
  }

  ngOnInit(): void {
  }

  onSubmit(): void{
    this.httpClient.post("/api/Student/AddNewStudent",
     {
       "Name": this.name,
       "Semester":  this.semester,
       "BirthDate": this.birthDate,
       "PhoneNumber": this.phoneNumber    
    }).subscribe(() => {
      this.router.navigateByUrl("/students");
    });
  }

}



interface NewStudent {
  name: string;
  semester: number;
  birthDate: string;
  phoneNumber: string
}