import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-student-page',
  templateUrl: './student-page.component.html',
  styleUrls: ['./student-page.component.css']
})
export class StudentPageComponent implements OnInit {

  private httpClient: HttpClient;
  public students?: Student[];

  constructor(http: HttpClient) {
      this.httpClient = http;
  }

  ngOnInit(): void {
    this.httpClient.get<Student[]>('/api/Student/GetAllStudents').subscribe(result => {
        this.students = result;
    }, error => console.error(error));
  }

}


interface Student {
  id: number;
  name: string;
  semester: number;
  birthDate: string;
  phoneNumber: string
}