import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentPageComponent } from './student-page/student-page.component';
import { GradePageComponent } from './grade-page/grade-page.component';
import { NewStudentPageComponent } from './new-student-page/new-student-page.component';

const routes: Routes = [
	{
		path: 'students',
		component: StudentPageComponent,
	},
	{
		path: 'new-student',
		component: NewStudentPageComponent,
	},
	{
		path: 'grades',
		component: GradePageComponent,
	},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }