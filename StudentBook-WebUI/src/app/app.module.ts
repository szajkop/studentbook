import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { StudentPageComponent } from './student-page/student-page.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GradePageComponent } from './grade-page/grade-page.component';
import { NewStudentPageComponent } from './new-student-page/new-student-page.component';

@NgModule({
  declarations: [
    AppComponent,
    StudentPageComponent,
    GradePageComponent,
    NewStudentPageComponent
  ],
  imports: [
      BrowserModule,
      HttpClientModule,
      AppRoutingModule,
      FormsModule,
      NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
