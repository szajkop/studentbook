﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StudentBook.Data.Migrations
{
    public partial class data_seed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "ID", "BirthDate", "Name", "PhoneNumber", "Semester" },
                values: new object[] { 1, new DateTime(1999, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Márk", 36401234444L, 2 });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "ID", "BirthDate", "Name", "PhoneNumber", "Semester" },
                values: new object[] { 2, new DateTime(2002, 4, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), "Renáta", 36401235555L, 1 });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "ID", "BirthDate", "Name", "PhoneNumber", "Semester" },
                values: new object[] { 3, new DateTime(2001, 8, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Norbert", 36401236666L, 3 });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "ID", "BirthDate", "Name", "PhoneNumber", "Semester" },
                values: new object[] { 4, new DateTime(2004, 4, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "Viktoria", 36401237777L, 1 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 1, 1, 2 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 2, 1, 4 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 3, 1, 4 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 4, 1, 3 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 5, 1, 5 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 6, 2, 5 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 7, 2, 5 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 8, 2, 3 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 9, 2, 5 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 10, 2, 1 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 11, 3, 4 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 12, 3, 2 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 13, 3, 3 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 14, 3, 5 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 15, 3, 5 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 16, 4, 4 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 17, 4, 5 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 18, 4, 4 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 19, 4, 1 });

            migrationBuilder.InsertData(
                table: "Grades",
                columns: new[] { "ID", "StudentID", "Value" },
                values: new object[] { 20, 4, 4 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Grades",
                keyColumn: "ID",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 4);
        }
    }
}
