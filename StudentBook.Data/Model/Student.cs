﻿namespace StudentBook.Data.Model
{
    using System.ComponentModel.DataAnnotations;

    public class Student
    {
        [Key]
        public int ID { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int Semester { get; set; }

        public virtual ICollection<Grade> Grades { get; set; }

        public DateTime BirthDate { get; set; }

        public long PhoneNumber { get; set; }
    }
}
