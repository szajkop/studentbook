﻿namespace StudentBook.Data.Model
{
    using System.ComponentModel.DataAnnotations;

    public class Grade
    {
        [Key]
        public int ID { get; set; }

        public int Value { get; set; }

        public int StudentID { get; set; }

        public virtual Student Student { get; set; }
    }
}
