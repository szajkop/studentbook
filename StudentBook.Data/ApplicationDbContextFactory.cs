﻿namespace StudentBook.Data
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;

    /// <summary>
    /// ApplicationDbContextFactory is used at generating schema migrations on design time.
    /// </summary>
    internal class ApplicationDbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        /// <summary>
        /// Creates the DbContext.
        /// </summary>
        /// <param name="args">args.</param>
        /// <returns>ApplicationDbContext.</returns>
        public ApplicationDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseSqlite("DataSource=:memory:");

            return new ApplicationDbContext(builder.Options);
        }
    }
}
