﻿namespace StudentBook.Data.Repository
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using StudentBook.Data.Repository.Interfaces;

    /// <summary>
    /// Generic interface of repository pattern.
    /// </summary>
    /// <typeparam name="TEntityData">generic type of entity.</typeparam>
    public class GenericRepository<TEntityData> : IGenericRepository<TEntityData>
        where TEntityData : class
    {
        private readonly DbSet<TEntityData> _dbSet;
        private readonly ApplicationDbContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{TEntity}"/> class.
        /// The constructor accepts a database context instance and initializes the dbset variable.
        /// </summary>
        /// <param name="context">dbcontext.</param>
        protected GenericRepository(ApplicationDbContext context)
        {
            this._context = context ?? throw new ArgumentNullException("context");
            this._dbSet = context.Set<TEntityData>();
        }

        /// <summary>
        /// Gets the entity by id.
        /// </summary>
        /// <param name="id">entityID.</param>
        /// <returns>entity matched by id or null if not found.</returns>
        public TEntityData GetById(int id)
        {
            return this._dbSet.Find(id);
        }

        /// <summary>
        /// Add a single entity to the context.
        /// </summary>
        /// <param name="entity">entity to add.</param>
        public virtual void Add(TEntityData entity)
        {
            if(!this.Validate(entity))
            {
                throw new ApplicationException("Validation failed!");
            }

            this._dbSet.Add(entity);
        }

        /// <summary>
        /// Deletes a defined entity.
        /// </summary>
        /// <param name="entity">entity to delete.</param>
        public void Remove(TEntityData entity)
        {
            this._dbSet.Remove(entity);
        }

        /// <summary>
        /// Updates the entity.
        /// </summary>
        /// <param name="entity">entity to update.</param>
        public void Update(TEntityData entity)
        {
            this._dbSet.Update(entity);
        }

        /// <summary>
        /// The Save method calls SaveChanges on the database context.
        /// </summary>
        /// <returns>Number of affected rows.</returns>
        public int Save()
        {
            return this._context.SaveChanges();
        }

        /// <summary>
        /// Gets all entities from the context as Queryable.
        /// </summary>
        /// <returns>entities as Queryable.</returns>
        public virtual IQueryable<TEntityData> GetAll()
        {
            return this._dbSet.AsQueryable();
        }

        /// <summary>
        /// Validates the entity. Throws exception when something fails.
        /// </summary>
        /// <param name="entity">Entity to validate.</param>
        /// <returns>True when valid.</returns>
        protected virtual bool Validate(TEntityData entity)
        {
            return true;
        }
    }
}
