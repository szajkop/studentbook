﻿namespace StudentBook.Data.Repository.Interfaces
{
    using StudentBook.Data.Model;

    /// <summary>
    /// Grade specific queries.
    /// </summary>
    public interface IGradeRepository : IGenericRepository<Grade>
    {
    }
}
