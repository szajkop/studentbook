﻿namespace StudentBook.Data.Repository.Interfaces
{
    using StudentBook.Data.Model;

    /// <summary>
    /// Student specific queries.
    /// </summary>
    public interface IStudentRepository : IGenericRepository<Student>
    {
        /// <summary>
        /// Gets all stored students, ordered by name.
        /// </summary>
        /// <returns>List of students.</returns>
        List<Student> GetAllStudents();
    }
}
