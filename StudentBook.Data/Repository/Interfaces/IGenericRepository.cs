﻿namespace StudentBook.Data.Repository.Interfaces
{
    /// <summary>
    /// Generic interface of repository pattern.
    /// </summary>
    /// <typeparam name="TEntity">generic type of entity.</typeparam>
    public interface IGenericRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Gets the entity by id.
        /// </summary>
        /// <param name="id">entityID.</param>
        /// <returns>entity matched by id or null if not found.</returns>
        TEntity GetById(int id);

        /// <summary>
        /// Add a single entity to the context.
        /// </summary>
        /// <param name="entity">entity to add.</param>
        void Add(TEntity entity);

        /// <summary>
        /// Deletes a defined entity.
        /// </summary>
        /// <param name="entity">entity to delete.</param>
        void Remove(TEntity entity);

        /// <summary>
        /// Updates the entity.
        /// </summary>
        /// <param name="entity">entity to update.</param>
        void Update(TEntity entity);

        /// <summary>
        /// The Save method calls SaveChanges on the database context.
        /// </summary>
        /// <returns>Number of affected rows.</returns>
        int Save();

        /// <summary>
        /// Gets all entities from the context as Queryable.
        /// </summary>
        /// <returns>entities as Queryable.</returns>
        IQueryable<TEntity> GetAll();
    }
}
