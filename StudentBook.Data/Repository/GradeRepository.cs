﻿namespace StudentBook.Data.Repository
{
    using StudentBook.Data.Model;
    using StudentBook.Data.Repository.Interfaces;

    /// <summary>
    /// Reposiotiry for File Statuses.
    /// </summary>
    public class GradeRepository : GenericRepository<Grade>, IGradeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GradeRepository"/> class.
        /// The constructor accepts a database context instance and initializes the dbset variable.
        /// </summary>
        /// <param name="context">dbcontext.</param>
        public GradeRepository(ApplicationDbContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Validates the entity. Throws exception when something fails.
        /// </summary>
        /// <param name="entity">Entity to validate.</param>
        /// <returns>True wh
        protected override bool Validate(Grade entity)
        {
            if(entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if(entity.Value < 1 || entity.Value > 5)
            {
                throw new ApplicationException("Grade value must be between 1 and 5");
            }

            return true;
        }
    }
}
