﻿namespace StudentBook.Data.Repository
{
    using Microsoft.EntityFrameworkCore;
    using StudentBook.Data.Model;
    using StudentBook.Data.Repository.Interfaces;
    using System.Collections.Generic;

    /// <summary>
    /// Reposiotiry for File Statuses.
    /// </summary>
    public class StudentRepository : GenericRepository<Student>, IStudentRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentRepository"/> class.
        /// The constructor accepts a database context instance and initializes the dbset variable.
        /// </summary>
        /// <param name="context">dbcontext.</param>
        public StudentRepository(ApplicationDbContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Gets all stored students, ordered by name.
        /// </summary>
        /// <returns>List of students.</returns>
        public List<Student> GetAllStudents()
        {
            return this.GetAll().Include(x => x.Grades).OrderBy(x => x.Name).ToList();
        }

        /// <summary>
        /// Validates the entity. Throws exception when something fails.
        /// </summary>
        /// <param name="entity">Entity to validate.</param>
        /// <returns>True wh
        protected override bool Validate(Student entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (entity.Semester < 0)
            {
                throw new ApplicationException("Semester value must be a positive number");
            }

            if (string.IsNullOrEmpty(entity.Name))
            {
                throw new ApplicationException("Name is empty");
            }

            var q = this.GetAll().FirstOrDefault(x => x.BirthDate == entity.BirthDate && x.Name == entity.Name);
            if(q != null)
            {
                throw new ApplicationException("Student already exists");
            }

            return true;
        }
    }
}
