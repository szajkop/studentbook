﻿namespace StudentBook.Data
{
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using StudentBook.Data.Model;

    /// <summary>
    /// Main data context.
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<IdentityUser, IdentityRole, string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationDbContext"/> class.
        /// </summary>
        /// <param name="options">DbContextOptions.</param>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            this.ChangeTracker.AutoDetectChangesEnabled = false;
        }

        /// <summary>
        /// Gets or sets the dbset for Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// Gets or sets the dbset for Grades.
        /// </summary>
        public DbSet<Grade> Grades { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            /*List<Student> students = new List<Student>();

            students.Add(new Student()
            {
                BirthDate = DateTime.ParseExact("1999.01.01", "yyyy.MM.dd", null),
                ID = 1,
                Name = "Márk",
                Semester = 2,
                PhoneNumber = 36401234444,
            });

            students.Add(new Student()
            {
                BirthDate = DateTime.ParseExact("2002.04.26", "yyyy.MM.dd", null),
                ID = 2,
                Name = "Renáta",
                Semester = 1,
                PhoneNumber = 36401235555,
            });

            students.Add(new Student()
            {
                BirthDate = DateTime.ParseExact("2001.08.10", "yyyy.MM.dd", null),
                ID = 3,
                Name = "Norbert",
                Semester = 3,
                PhoneNumber = 36401236666,
            });

            students.Add(new Student()
            {
                BirthDate = DateTime.ParseExact("2004.04.14", "yyyy.MM.dd", null),
                ID = 4,
                Name = "Viktoria",
                Semester = 1,
                PhoneNumber = 36401237777,
            });

            modelBuilder.Entity<Student>().HasData(students);

            List <Grade> grades = new List<Grade>();

            int idx = 1;
            grades.Add(new Grade() { ID = idx++, Value = 2, StudentID = students[0].ID });
            grades.Add(new Grade() { ID = idx++, Value = 4, StudentID = students[0].ID });
            grades.Add(new Grade() { ID = idx++, Value = 4, StudentID = students[0].ID });
            grades.Add(new Grade() { ID = idx++, Value = 3, StudentID = students[0].ID });
            grades.Add(new Grade() { ID = idx++, Value = 5, StudentID = students[0].ID });

            grades.Add(new Grade() { ID = idx++, Value = 5, StudentID = students[1].ID });
            grades.Add(new Grade() { ID = idx++, Value = 5, StudentID = students[1].ID });
            grades.Add(new Grade() { ID = idx++, Value = 3, StudentID = students[1].ID });
            grades.Add(new Grade() { ID = idx++, Value = 5, StudentID = students[1].ID });
            grades.Add(new Grade() { ID = idx++, Value = 1, StudentID = students[1].ID });

            grades.Add(new Grade() { ID = idx++, Value = 4, StudentID = students[2].ID });
            grades.Add(new Grade() { ID = idx++, Value = 2, StudentID = students[2].ID });
            grades.Add(new Grade() { ID = idx++, Value = 3, StudentID = students[2].ID });
            grades.Add(new Grade() { ID = idx++, Value = 5, StudentID = students[2].ID });
            grades.Add(new Grade() { ID = idx++, Value = 5, StudentID = students[2].ID });

            grades.Add(new Grade() { ID = idx++, Value = 4, StudentID = students[3].ID });
            grades.Add(new Grade() { ID = idx++, Value = 5, StudentID = students[3].ID });
            grades.Add(new Grade() { ID = idx++, Value = 4, StudentID = students[3].ID });
            grades.Add(new Grade() { ID = idx++, Value = 1, StudentID = students[3].ID });
            grades.Add(new Grade() { ID = idx++, Value = 4, StudentID = students[3].ID });

            modelBuilder.Entity<Grade>().HasData(grades);


            modelBuilder.Entity<Grade>().HasData(grades);*/
        }
    }
}
