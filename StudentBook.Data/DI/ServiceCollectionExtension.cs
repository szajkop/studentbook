﻿namespace StudentBook.Data.DI
{
    using Microsoft.Data.Sqlite;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using StudentBook.Data.Repository;
    using StudentBook.Data.Repository.Interfaces;

    /// <summary>
    /// Extension method to work in the IServiceCollection.
    /// </summary>
    public static class ServiceCollectionExtension
    {
        /// <summary>
        /// Method for mappings from the interfaces to the concrete classes or all the other registrations to the DI container.
        /// </summary>
        /// <param name="services">Service collection.</param>
        /// <returns>Service collection with the new dependencies.</returns>
        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services.AddScoped<IGradeRepository, GradeRepository>();
            services.AddScoped<IStudentRepository, StudentRepository>();

            var inMemorySqlite = new SqliteConnection("Data Source=:memory:");
            inMemorySqlite.Open();

            services.AddDbContext<ApplicationDbContext>(opt => opt
                .UseSqlite(inMemorySqlite));
            return services;
        }
    }
}
