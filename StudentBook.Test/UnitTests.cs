using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using StudentBook.API.Services;
using StudentBook.API.Services.Interfaces;
using StudentBook.Data;
using StudentBook.Data.Model;
using StudentBook.Data.Repository;
using StudentBook.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudentBook.Test
{
    public class UnitTests
    {
        private IStudentRepository studentRepo;
        private IGradeRepository gradeRepo;

        private List<Student> students;

        [SetUp]
        public void Setup()
        {
            List<Student> students = new List<Student>();

            students.Add(new Student()
            {
                BirthDate = DateTime.ParseExact("1999.01.01", "yyyy.MM.dd", null),
                Name = "CTestUser",
                Semester = 2,
                PhoneNumber = 36401234444,
                Grades = new List<Grade>()
            });

            students.Add(new Student()
            {
                BirthDate = DateTime.ParseExact("2002.04.26", "yyyy.MM.dd", null),
                Name = "ATestUser",
                Semester = 1,
                PhoneNumber = 36401235555,
                Grades = new List<Grade>()
            });

            students.Add(new Student()
            {
                BirthDate = DateTime.ParseExact("2001.08.10", "yyyy.MM.dd", null),
                Name = "BTestUser",
                Semester = 3,
                PhoneNumber = 36401236666,
                Grades = new List<Grade>()
            });

            List<Grade> grades = new List<Grade>();

            students[0].Grades.Add(new Grade() { Value = 2, Student = students[0] });
            students[0].Grades.Add(new Grade() { Value = 4, Student = students[0] });
            students[0].Grades.Add(new Grade() { Value = 4, Student = students[0] });
            students[0].Grades.Add(new Grade() { Value = 3, Student = students[0] });
            students[0].Grades.Add(new Grade() { Value = 5, Student = students[0] });

            students[1].Grades.Add(new Grade() { Value = 5, Student = students[1] });
            students[1].Grades.Add(new Grade() { Value = 5, Student = students[1] });
            students[1].Grades.Add(new Grade() { Value = 3, Student = students[1] });
            students[1].Grades.Add(new Grade() { Value = 5, Student = students[1] });
            students[1].Grades.Add(new Grade() { Value = 1, Student = students[1] });

            students[2].Grades.Add(new Grade() { Value = 4, Student = students[2] });
            students[2].Grades.Add(new Grade() { Value = 2, Student = students[2] });
            students[2].Grades.Add(new Grade() { Value = 3, Student = students[2] });
            students[2].Grades.Add(new Grade() { Value = 5, Student = students[2] });
            students[2].Grades.Add(new Grade() { Value = 5, Student = students[2] });

            this.students = students;


            var inMemorySqlite = new SqliteConnection("Data Source=:memory:");
            inMemorySqlite.Open();

            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseSqlite(inMemorySqlite);

            var dbContext = new ApplicationDbContext(builder.Options);

            dbContext.Database.Migrate();

            var studentMock = new Mock<StudentRepository>(dbContext)
            {
                CallBase = true
            };

            this.studentRepo = studentMock.Object;

            var gradeMock = new Mock<GradeRepository>(dbContext)
            {
                CallBase = true
            };

            this.gradeRepo = gradeMock.Object;

            foreach (var item in students)
            {
                this.studentRepo.Add(item);
            }

            this.studentRepo.Save();

        }

        [Test]
        public void GetStudentsOrderedTest()
        {
            var q = this.studentRepo.GetAllStudents();
            Assert.IsTrue(q.First().Name == "ATestUser");
        }

        [Test]
        public void CreateNewStudentTest()
        {
            var student = new Student()
            {
                BirthDate = DateTime.ParseExact("2004.04.14", "yyyy.MM.dd", null),
                Name = "TestUser1",
                Semester = 1,
                PhoneNumber = 36401237777,
                Grades = new List<Grade>()
            };

            this.studentRepo.Add(student);
            this.studentRepo.Save();

            Assert.IsTrue(this.studentRepo.GetAllStudents().Any(x=>x.Name == "TestUser1"));
            Assert.That(student.ID, Is.Not.EqualTo(0));
        }

        [Test]
        public void CreateDuplicatedStudentTest()
        {
            var student = new Student()
            {
                BirthDate = DateTime.ParseExact("2004.04.14", "yyyy.MM.dd", null),
                Name = "CreateDuplicatedStudentTest",
                Semester = 1,
                PhoneNumber = 36401237777,
                Grades = new List<Grade>()
            };

            var student2 = new Student()
            {
                BirthDate = DateTime.ParseExact("2004.04.14", "yyyy.MM.dd", null),
                Name = "CreateDuplicatedStudentTest",
                Semester = 1,
                PhoneNumber = 36401237777,
                Grades = new List<Grade>()
            };

            this.studentRepo.Add(student);
            this.studentRepo.Save();


            Assert.Throws<ApplicationException>(() => this.studentRepo.Add(student2));
        }

        [Test]
        public void CreateInvalidStudentNameTest()
        {
            var student = new Student()
            {
                BirthDate = DateTime.ParseExact("2004.04.14", "yyyy.MM.dd", null),
                Semester = 1,
                PhoneNumber = 36401237777,
                Grades = new List<Grade>()
            };

            Assert.Throws<ApplicationException>(() => this.studentRepo.Add(student));
        }

        [Test]
        public void CreateInvalidStudentSemesterTest()
        {
            var student = new Student()
            {
                BirthDate = DateTime.ParseExact("2004.04.14", "yyyy.MM.dd", null),
                Semester = -1,
                Name = "CreateInvalidStudentSemesterTest",
                PhoneNumber = 36401237777,
                Grades = new List<Grade>()
            };

            Assert.Throws<ApplicationException>(() => this.studentRepo.Add(student));
        }

        [Test]
        public void AddGradeToStudentTest()
        {
            var student = this.students.First();

            var grade = new Grade()
            {
                StudentID = student.ID,
                Value = 4,
            };

            this.gradeRepo.Add(grade);
            this.gradeRepo.Save();

            Assert.That(grade.ID, Is.Not.EqualTo(0));
        }

        [Test]
        public void AddInvalidGradeToStudentTest()
        {
            var student = this.students.First();

            var grade = new Grade()
            {
                StudentID = student.ID,
                Value = -1,
            };

            Assert.Throws<ApplicationException>(() => this.gradeRepo.Add(grade));

            grade.Value = 100;
            Assert.Throws<ApplicationException>(() => this.gradeRepo.Add(grade));
        }

        [Test]
        public void CalculateStatisticsTest()
        {
            IStudentStatisticsService statisticsService = new StudentStatisticsService();
            var student = this.students.First(x => x.Name == "ATestUser");

            var stats = statisticsService.CalculateStatisticsForStudent(student);

            Assert.That(stats.FailingGradeCount, Is.EqualTo(1));
            Assert.That(stats.GradesAvg, Is.EqualTo(3.8));
            Assert.That(stats.BestGrade, Is.EqualTo(5));
        }
    }
}