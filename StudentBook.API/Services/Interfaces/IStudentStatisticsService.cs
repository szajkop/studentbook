﻿namespace StudentBook.API.Services.Interfaces
{
    using StudentBook.API.Model;
    using StudentBook.Data.Model;
    using System.Collections.Generic;

    public interface IStudentStatisticsService
    {
        StudentStatistics? CalculateStatisticsForStudent(Student student);

        List<StudentStatistics> CalculateStatisticsForStudents(IEnumerable<Student> students);
    }
}
