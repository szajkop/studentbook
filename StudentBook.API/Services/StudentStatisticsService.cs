﻿namespace StudentBook.API.Services
{
    using StudentBook.API.Model;
    using StudentBook.API.Services.Interfaces;
    using StudentBook.Data.Model;
    using System.Collections.Generic;

    public class StudentStatisticsService : IStudentStatisticsService
    {
        public StudentStatistics? CalculateStatisticsForStudent(Student s)
        {
            if(s == null)
            {
                throw new ArgumentNullException(nameof(s));
            }

            if(s.Grades == null || s.Grades.Count == 0)
            {
                return null;
            }

            return new StudentStatistics()
            {
                GradesAvg = s.Grades.Average(x => x.Value),
                BestGrade = s.Grades.Max(x => x.Value),
                FailingGradeCount = s.Grades.Count(x => x.Value == 1),
                Name = s.Name,
            };
        }

        public List<StudentStatistics> CalculateStatisticsForStudents(IEnumerable<Student> students)
        {
            List<StudentStatistics> studentStatistics = new List<StudentStatistics>();

            foreach (var item in students)
            {
                var stat = this.CalculateStatisticsForStudent(item);

                if(stat == null)
                {
                    continue;
                }

                studentStatistics.Add(stat);
            }

            return studentStatistics;
        }
    }
}
