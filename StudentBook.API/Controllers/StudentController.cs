﻿namespace StudentBook.API.Controllers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using StudentBook.API.Model.Transfer;
    using StudentBook.API.Services.Interfaces;
    using StudentBook.Data.Model;
    using StudentBook.Data.Repository.Interfaces;

    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IStudentRepository _studentRepo;
        private readonly IGradeRepository _gradeRepo;
        private readonly IStudentStatisticsService _statisticsService;

        public StudentController(
            IMapper mapper,
            IStudentRepository studentRepository,
            IGradeRepository gradeRepository,
            IStudentStatisticsService statisticsService)
        {
            this._mapper = mapper;
            this._studentRepo = studentRepository;
            this._gradeRepo = gradeRepository;
            this._statisticsService = statisticsService;
        }

        [Route("GetAllStudents")]
        [HttpGet]
        public ActionResult<IEnumerable<StudentReadDto>> GetAllStudents()
        {
            var q = this._studentRepo.GetAllStudents();
            return Ok(_mapper.Map<IEnumerable<StudentReadDto>>(q));
        }

        [Route("AddNewStudent")]
        //[Authorize]
        [HttpPost]
        public ActionResult<StudentReadDto> AddNewStudent([FromForm] StudentCreateDto studentCreateDto)
        {
            try
            {
                var student = this._mapper.Map<Student>(studentCreateDto);
                this._studentRepo.Add(student);
                this._studentRepo.Save();

                return Ok(this._mapper.Map<StudentReadDto>(student));
            }
            catch (Exception ex)
            {
                return Problem(detail: "Error occured: " + ex.Message);
            }
        }

        [Route("AddGradeForStudent")]
        [HttpPost]
        public ActionResult<GradeReadDto> AddGradeForStudent([FromForm] StudentAddGradeDto studentAddGradeDto)
        {
            try
            {
                var q = this._studentRepo.GetById(studentAddGradeDto.StudentID);
                if(q == null)
                {
                    throw new ApplicationException($"User with id: {studentAddGradeDto.StudentID} not found!");
                }

                var grade = this._mapper.Map<Grade>(studentAddGradeDto);

                this._gradeRepo.Add(grade);
                this._gradeRepo.Save();

                return Ok(this._mapper.Map<GradeReadDto>(grade));
            }
            catch (Exception ex)
            {
                return Problem(detail: "Error occured: " + ex.Message);
            }
        }

        [Route("GetStudentStatistics")]
        [HttpGet]
        public ActionResult<IEnumerable<StatisticsReadDto>> GetStudentStatistics()
        {
            var stats = this._statisticsService.CalculateStatisticsForStudents(this._studentRepo.GetAllStudents());
            return Ok(_mapper.Map<IEnumerable<StatisticsReadDto>>(stats).OrderByDescending(x => x.GradesAvg));
        }
    }
}
