﻿namespace StudentBook.API.Model
{
    public class StudentStatistics
    {
        public string Name { get; set; }

        public double GradesAvg { get; set; }

        public int BestGrade { get; set; }

        public int FailingGradeCount { get; set; }
    }
}
