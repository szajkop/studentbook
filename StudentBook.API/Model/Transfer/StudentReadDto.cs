﻿namespace StudentBook.API.Model.Transfer
{
    using Newtonsoft.Json;

    public class StudentReadDto
    {
        [JsonProperty("id")]
        public int ID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("semester")]
        public int Semester { get; set; }

        [JsonProperty("birthDate")]
        public DateTime BirthDate { get; set; }

        [JsonProperty("phoneNumber")]
        public long PhoneNumber { get; set; }
    }
}
