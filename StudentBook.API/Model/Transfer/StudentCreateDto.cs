﻿namespace StudentBook.API.Model.Transfer
{
    using Newtonsoft.Json;
    using System.ComponentModel.DataAnnotations;

    public class StudentCreateDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int Semester { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        [Required]
        public long PhoneNumber { get; set; }
    }
}
