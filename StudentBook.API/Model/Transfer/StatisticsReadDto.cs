﻿namespace StudentBook.API.Model.Transfer
{
    using Newtonsoft.Json;

    public class StatisticsReadDto
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("gradesAvg")]
        public double GradesAvg { get; set; }

        [JsonProperty("bestGrade")]
        public int BestGrade { get; set; }

        [JsonProperty("failingGradeCount")]
        public int FailingGradeCount { get; set; }
    }
}
