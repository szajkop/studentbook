﻿namespace StudentBook.API.Model.Transfer
{
    using Newtonsoft.Json;

    public class GradeReadDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("studentId")]
        public int StudentId { get; set; }

        [JsonProperty("value")]
        public int Value { get; set; }
    }
}
