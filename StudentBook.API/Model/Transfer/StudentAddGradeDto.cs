﻿namespace StudentBook.API.Model.Transfer
{
    using System.ComponentModel.DataAnnotations;

    public class StudentAddGradeDto
    {
        [Required]
        public int StudentID { get; set; }

        [Required]
        public int Value { get; set; }
    }
}
