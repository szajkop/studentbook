﻿namespace StudentBook.API.Mapper
{
    using AutoMapper;
    using StudentBook.API.Model;
    using StudentBook.API.Model.Transfer;

    public class StatisticsProfile : Profile
    {
        public StatisticsProfile()
        {
            // Source -> target
            CreateMap<StudentStatistics, StatisticsReadDto>();
        }
    }
}
