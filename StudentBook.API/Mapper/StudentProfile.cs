﻿namespace StudentBook.API.Mapper
{
    using AutoMapper;
    using StudentBook.API.Model.Transfer;
    using StudentBook.Data.Model;

    public class StudentProfile : Profile
    {
        public StudentProfile()
        {
            // Source -> target
            CreateMap<Student, StudentReadDto>();
            CreateMap<StudentCreateDto, Student>();
        }
    }
}
