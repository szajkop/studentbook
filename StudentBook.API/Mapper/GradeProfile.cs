﻿namespace StudentBook.API.Mapper
{
    using AutoMapper;
    using StudentBook.API.Model.Transfer;
    using StudentBook.Data.Model;

    public class GradeProfile : Profile
    {
        public GradeProfile()
        {
            // Source -> target
            CreateMap<StudentAddGradeDto, Grade>();
            CreateMap<Grade, GradeReadDto>();
        }
    }
}
